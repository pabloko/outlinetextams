// IRPluginObject.h: interface for the CIRPluginObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IRPLUGINOBJECT_H__345EA06E_203A_4565_990A_B6A70C5B6B9E__INCLUDED_)
#define AFX_IRPLUGINOBJECT_H__345EA06E_203A_4565_990A_B6A70C5B6B9E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define IR_PLUGIN_CLASS_VERSION 1

#define IR_PLUGIN_EVENT_NAME_MAX 200
#define IR_PLUGIN_EVENT_ARGS_MAX 1000
#include <gdiplus.h>
enum TextEffect
{
	TextGlow,
	SingleOutline,
	DblOutline,
	GradOutline,
	NoOutline,
	OnlyOutline,
	DblGlow
};
extern char _[1];
#ifdef SDK_DLL_EXPORT
struct __declspec(dllexport) IRPluginEventInfo
#else
struct IRPluginEventInfo
#endif
{
	char m_szName[IR_PLUGIN_EVENT_NAME_MAX];
	char m_szArgs[IR_PLUGIN_EVENT_ARGS_MAX];
};
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define LOG(...) Log("Trace: %s [%s](%d)",__FUNCTION__,__FILENAME__,__LINE__); if (#__VA_ARGS__>0) Log(__VA_ARGS__)
#ifdef NDEBUG
#define LOG(x) {}
#endif
#define NUM_OB_EVENTS 3

#ifdef SDK_DLL_EXPORT
class __declspec(dllexport) CIRPluginObject
#else
extern void Log(CHAR * szFormat, ...);
class CIRPluginObject
#endif
{
public:
	CIRPluginObject();
	virtual ~CIRPluginObject();

	// Informational:
	virtual void GetDefaultSize(SIZE* pSize) { pSize->cx = 280; pSize->cy = 60; };
	virtual BOOL IsWindowedObject() { return FALSE; };
	virtual HWND GetWindowHandle();

	virtual void DrawDesign(HDC hDC, HWND hMainWnd, RECT rcObRect, BOOL bVisible, BOOL bEnabled);
	virtual void DrawRuntime(HDC hDC, HWND hMainWnd, RECT rcObRect, BOOL bVisible, BOOL bEnabled);
	virtual void HideInternalObject();
	virtual int GetCustomProperties(char* szBuffer, int* pnBufferSize);
	virtual void SetCustomProperties(char* szPropsList);
	virtual BOOL ShowProperties(char* szPluginFolder);
	virtual BOOL GetEvent(int nIndex, IRPluginEventInfo* pEventInfo);
	virtual int GetNumEvents();
	virtual int RegisterLUAFunctions(lua_State* L);
	virtual BOOL LetAMSHandleCursorChange() { return FALSE; };
	virtual BOOL LetAMSHandleSounds() { return FALSE; };
	virtual BOOL LetAMSHandleTooltip() { return FALSE; };
	virtual BOOL CanSetFocus() { return FALSE; };
	virtual void DoSetFocus();
	virtual void ShowWindow(BOOL bVisible) { LOG(_); };

	// Events from above...
	virtual void OnMouseOver(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_);  FireEvent("On Enter", ""); };
	virtual void OnMouseLeave(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_); FireEvent("On Leave", ""); };
	virtual void OnLBtnDown(HWND hWndParent, POINT ptMousePos, RECT rcObRect) {LOG(_);};
	virtual void OnLBtnUp(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_); FireEvent("On Click", ""); };
	virtual void OnLBtnDoubleClick(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_); };
	virtual void OnRBtnDown(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_); };
	virtual void OnRBtnUp(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_); };
	virtual void OnRBtnDoubleClick(HWND hWndParent, POINT ptMousePos, RECT rcObRect) { LOG(_); };

	void FireEvent(LPCTSTR strEventName, LPCTSTR strArguments);

	// Non-Virtuals
	int GetObjectID(char* szBuffer, int* pnBufferSize);

	lua_State* m_pLuaState;

protected:
	char m_szObjectID[100];
	IRPluginEventInfo* m_pEvents[NUM_OB_EVENTS];
	//char m_szPropertiesString[1000];
	
public:
	int m_Width, m_Height, m_fsize, outalpha1, outalpha2, outalpha3, alpha1, alpha2, outthikn1, outthikn2, outthikn3, shaoffx, shaoffy;
	Gdiplus::FontStyle m_fstyle;
	Gdiplus::LinearGradientMode gradto;
	bool has_gradient, shaen, shadiffuse, shaextrude;
	HWND m_hWnd;
	TextEffect m_mode;
	wchar_t pszbuf[255], pszfontfam[60];
	//RECT rcr;
	char* m_szProperties;
	Gdiplus::Color color1,color2,outcolor1,outcolor2,outcolor3;
	//OutlineText* m_OutlineText;
};

// Some useful functions...
CIRPluginObject* IRLUA_PLUGIN_GetObjectPtr(lua_State *luaState, LPCTSTR strObjectName);
void IRLUA_PLUGIN_RedrawObject(lua_State *luaState, LPCTSTR strObjectName);

#endif // !defined(AFX_IRPLUGINOBJECT_H__345EA06E_203A_4565_990A_B6A70C5B6B9E__INCLUDED_)
