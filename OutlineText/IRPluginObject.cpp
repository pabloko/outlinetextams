﻿#include "stdafx.h"
#include "IRPluginObject.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* encode(wchar_t* s)
{
	//OutputDebugString("ENCODING");
	//OutputDebugStringW(s);
	DWORD siz;
	CryptBinaryToStringW((byte*)s, lstrlenW(s)* sizeof(wchar_t), CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, NULL, &siz);
	wchar_t* ret = (wchar_t*)malloc(siz* sizeof(wchar_t)); char* out = (char*)malloc(siz* sizeof(wchar_t));
	CryptBinaryToStringW((byte*)s, lstrlenW(s)* sizeof(wchar_t), CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, ret, &siz);
	WideCharToMultiByte(CP_UTF8, 0, ret, lstrlenW(ret)- sizeof(char), out, lstrlenW(ret)- sizeof(char), 0, 0);
	out[(lstrlenW(ret) - sizeof(wchar_t))] = '\0'; delete ret;
	//OutputDebugString(out);
	return out;
}

wchar_t* decode(char* s)
{
	//OutputDebugString("DECODING");
	//OutputDebugString(s);
	wchar_t* ret = (wchar_t*)malloc((strlen(s) * sizeof(wchar_t)) + sizeof(wchar_t));
	wchar_t* out = (wchar_t*)malloc((strlen(s) * sizeof(wchar_t)) + sizeof(wchar_t));
	MultiByteToWideChar(CP_ACP, 0, s, strlen(s), ret, strlen(s)); ret[strlen(s)] = '\0'; DWORD siz;
	CryptStringToBinaryW(ret, lstrlenW(ret), CRYPT_STRING_BASE64, (byte*)out, &siz, NULL, NULL);
	out[(siz / sizeof(wchar_t))] = '\0'; delete ret;
	//OutputDebugStringW(out);
	return out;
}

void SetEffects(TextDesigner::IOutlineText* pOutlineText, Gdiplus::Brush* pBrush, CIRPluginObject* cip) {
	using namespace Gdiplus;
	if (pBrush == NULL) {
		if (cip->m_mode == SingleOutline)
			pOutlineText->TextOutline(cip->color1, cip->outcolor1, cip->outthikn1);
		else if (cip->m_mode == DblOutline)
			pOutlineText->TextDblOutline(cip->color1, cip->outcolor1, cip->outcolor2, cip->outthikn1, cip->outthikn2);
		else if (cip->m_mode == TextGlow)
			pOutlineText->TextGlow(cip->color1, cip->outcolor1, cip->outthikn1);
		else if (cip->m_mode == GradOutline)
			pOutlineText->TextGradOutline(cip->color1, cip->outcolor1, cip->outcolor2, cip->outthikn1 + cip->outthikn2, TextDesigner::GradientType::Linear);
		else if (cip->m_mode == NoOutline)
			pOutlineText->TextNoOutline(cip->color1);
		else if (cip->m_mode == OnlyOutline)
			pOutlineText->TextOnlyOutline(cip->outcolor1, cip->outthikn1, false);
		else if (cip->m_mode == DblGlow)
			pOutlineText->TextDblGlow(cip->color1, cip->outcolor1, cip->outcolor2, cip->outthikn1, cip->outthikn2);
	} else {
		if (cip->m_mode == SingleOutline)
			pOutlineText->TextOutline(*pBrush, cip->outcolor1, cip->outthikn1);
		else if (cip->m_mode == DblOutline)
			pOutlineText->TextDblOutline(*pBrush, cip->outcolor1, cip->outcolor2, cip->outthikn1, cip->outthikn2);
		else if (cip->m_mode == TextGlow)
			pOutlineText->TextGlow(*pBrush, cip->outcolor1, cip->outthikn1);
		else if (cip->m_mode == GradOutline)
			pOutlineText->TextGradOutline(*pBrush, cip->outcolor1, cip->outcolor2, cip->outthikn1 + cip->outthikn2, TextDesigner::GradientType::Linear);
		else if (cip->m_mode == NoOutline)
			pOutlineText->TextNoOutline(*pBrush);
		else if (cip->m_mode == OnlyOutline)
			pOutlineText->TextOnlyOutline(cip->outcolor1, cip->outthikn1, false);
		else if (cip->m_mode == DblGlow)
			pOutlineText->TextDblGlow(*pBrush, cip->outcolor1, cip->outcolor2, cip->outthikn1, cip->outthikn2);
	}
}

CIRPluginObject::CIRPluginObject()
{
	LOG(_);
	memset(m_szObjectID, 0, 100);
	lstrcpy(m_szObjectID, "OBJECT_PLUGIN_OUTLNT");
	for (int i = 0; i < NUM_OB_EVENTS; i++)
		m_pEvents[i] = NULL;
	m_pEvents[0] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[0]->m_szName, "On Click");
	lstrcpy(m_pEvents[0]->m_szArgs, "");
	m_pEvents[1] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[1]->m_szName, "On Enter");
	lstrcpy(m_pEvents[1]->m_szArgs, "");
	m_pEvents[2] = new IRPluginEventInfo;
	lstrcpy(m_pEvents[2]->m_szName, "On Leave");
	lstrcpy(m_pEvents[2]->m_szArgs, "");
	wchar_t* dcd = decode("TwB1AHQAbABpAG4AZQBUAGUAeAB0AA==");
	wsprintfW(pszbuf, dcd);
	delete dcd;
	m_szProperties = strdup("TwB1AHQAbABpAG4AZQBUAGUAeAB0AA==\n255\n128\n64\n200\n0\n0\n1\n48\nArial\n0\n1\n1\n255\n255\n200\n0\n0\n255\n1\n0\n200\n0\n255\n1\n1\n0\n0\n200\n255\n1\n3\n3\n0\n1");
	color1 = Gdiplus::Color(255, 128, 64);
	color2 = Gdiplus::Color(150, 0, 0);
	gradto = (Gdiplus::LinearGradientMode)1;
	m_fsize = 48;
	wsprintfW(pszfontfam, L"Arial");
	m_fstyle = (Gdiplus::FontStyle)0;
	has_gradient = true;
	m_mode = (TextEffect)1;
	alpha1 = 255;
	alpha2 = 255;
	outcolor1 = Gdiplus::Color(200, 0, 0); 
	outalpha1 = 255;
	outthikn1 = 1;
	outcolor2 = Gdiplus::Color(0, 200, 0);
	outalpha2 = 255;
	outthikn2 = 1;
	shaen = true;
	outcolor3 = Gdiplus::Color(0, 0, 200);
	outalpha3 = 255;
	outthikn3 = 1;
	shaoffx = 3; 
	shaoffy = 3;
	shadiffuse = false;
	shaextrude = true;
}

CIRPluginObject::~CIRPluginObject()
{
	LOG(_);
	HideInternalObject();
}

HWND CIRPluginObject::GetWindowHandle()
{
	LOG(_);
	return NULL;
}

void CIRPluginObject::DrawDesign(HDC hDC, HWND hMainWnd, RECT rcObRect, BOOL bVisible, BOOL bEnabled)
{
	LOG(_);
	if (m_hWnd==NULL) 
		m_hWnd = hMainWnd;

	if (bVisible)
	{
		//Log("DrawDesign ParentHwnd=%x x=%d y=%d width=%d height=%d", hMainWnd, rcObRect.left, rcObRect.top, rcObRect.right - rcObRect.left, rcObRect.bottom - rcObRect.top);
		using namespace Gdiplus;
		using namespace TextDesigner;
		Gdiplus::Graphics* graphics = Gdiplus::Graphics::FromHDC(hDC);
		graphics->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeAntiAlias);
		graphics->SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic);
		FontFamily fontFamily(pszfontfam);
		StringFormat strformat;
		OutlineText text;
		SetEffects(&text, NULL, this);
		text.EnableShadow(shaen);

		if (shaen) {
			if (shaextrude)
				text.Extrude(outcolor3, outthikn3, Gdiplus::Point(shaoffx, shaoffy));
			else if (shadiffuse)
				text.DiffusedShadow(outcolor3, outthikn3, Gdiplus::Point(shaoffx, shaoffy));
			else
				text.Shadow(outcolor3, outthikn3, Gdiplus::Point(shaoffx, shaoffy));
		}
		if (has_gradient) {
			float fStartX, fStartY, fDestWidth, fDestHeight;
			///text.GdiMeasureString(graphics, &myfont, pszbuf, Gdiplus::Point(rcObRect.left, rcObRect.top), &fStartX, &fStartY, &fDestWidth, &fDestHeight);
			text.MeasureString(graphics, &fontFamily, m_fstyle, m_fsize, pszbuf, Gdiplus::Point(0, 0), &strformat, &fStartX, &fStartY, &fDestWidth, &fDestHeight);
			Gdiplus::Rect trect(fStartX, fStartY, fDestWidth - fStartX, fDestHeight - fStartY);
			//Log("RECT IS %d %d - %d %d", trect.X, trect.Y, trect.Width, trect.Height);

			trect.Width += trect.X;
			trect.Height += trect.Y;
			trect.X += rcObRect.left;
			trect.Y += rcObRect.top;

			Gdiplus::LinearGradientBrush* m_pGradientBrush = new Gdiplus::LinearGradientBrush(trect, color1, color2, gradto);
			graphics->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeHalf);
			SetEffects(&text, m_pGradientBrush, this);
		} 
		///text.GdiDrawString(graphics, &myfont, pszbuf, Gdiplus::Point(rcObRect.left, rcObRect.top));
		text.DrawString(graphics, &fontFamily, m_fstyle, m_fsize, pszbuf, Gdiplus::Point(rcObRect.left, rcObRect.top), &strformat);
	}
}

void CIRPluginObject::DrawRuntime(HDC hDC, HWND hMainWnd, RECT rcObRect, BOOL bVisible, BOOL bEnabled)
{
	LOG(_);;
	DrawDesign(hDC, hMainWnd, rcObRect, bVisible, bEnabled);
}

void CIRPluginObject::HideInternalObject()
{
	LOG(_);
}

int CIRPluginObject::GetCustomProperties(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	guiobj = this; /////////////////

	int nLength = lstrlen(m_szProperties);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, m_szProperties);
		return nLength;
	}
}

void CIRPluginObject::SetCustomProperties(char* szPropsList)
{
	LOG(szPropsList);
	delete m_szProperties;
	m_szProperties = strdup(szPropsList);
	int nTokenNum = 0;
	char *token;
	int col[3];
	token = strtok(szPropsList, "\n");
	while (token != NULL)
	{
		switch (nTokenNum)
		{
		case 0:
		{
			delete pszbuf;
			wchar_t* dcd = decode(token);
			wsprintfW(pszbuf, dcd);
			delete dcd;
		}
			break;
		case 1:
			col[0] = atoi(token);
			break; 
		case 2:
			col[1] = atoi(token);
			break;
		case 3:
			col[2] = atoi(token);
			color1 = Gdiplus::Color(col[0], col[1], col[2]);
			break;
		case 4:
			col[0] = atoi(token);
			break;
		case 5:
			col[1] = atoi(token);
			break;
		case 6:
			col[2] = atoi(token);
			color2 = Gdiplus::Color(col[0], col[1], col[2]);
			break;
		case 7:
			gradto = (Gdiplus::LinearGradientMode)atoi(token);
			break;
		case 8:
			m_fsize = atoi(token);
			break;
		case 9:
			MultiByteToWideChar(CP_ACP, NULL, token, strlen(token), pszfontfam, strlen(token) + 1);
			pszfontfam[strlen(token)] = '\0';
			break;
		case 10:
			m_fstyle = (Gdiplus::FontStyle)atoi(token);
			break;
		case 11:
			has_gradient = atoi(token)==1?true:false;
			break;
		case 12:
			m_mode = (TextEffect)atoi(token);
			break;
		case 13:
			alpha1 = atoi(token);
			if (alpha1 != 255) color1 = Gdiplus::Color(alpha1, color1.GetR(), color1.GetG(), color1.GetB());
			break;
		case 14:
			alpha2 = atoi(token);
			if (alpha2 != 255) color1 = Gdiplus::Color(alpha2, color2.GetR(), color2.GetG(), color2.GetB());
			break;
		case 15:
			col[0] = atoi(token);
			break;
		case 16:
			col[1] = atoi(token);
			break;
		case 17:
			col[2] = atoi(token);
			outcolor1 = Gdiplus::Color(col[0], col[1], col[2]);
			break;
		case 18:
			outalpha1 = atoi(token);
			if (outalpha1 != 255) outcolor1 = Gdiplus::Color(outalpha1, col[0], col[1], col[2]);
			break;
		case 19:
			outthikn1 = atoi(token);
			break;
		case 20:
			col[0] = atoi(token);
			break;
		case 21:
			col[1] = atoi(token);
			break;
		case 22:
			col[2] = atoi(token);
			outcolor2 = Gdiplus::Color(col[0], col[1], col[2]);
			break;
		case 23:
			outalpha2 = atoi(token);
			if (outalpha2 != 255) outcolor2 = Gdiplus::Color(outalpha2, col[0], col[1], col[2]);
			break;
		case 24:
			outthikn2 = atoi(token);
			break;
		case 25:
			shaen = atoi(token) == 1 ? true : false;
			break;
		case 26:
			col[0] = atoi(token);
			break;
		case 27:
			col[1] = atoi(token);
			break;
		case 28:
			col[2] = atoi(token);
			outcolor3 = Gdiplus::Color(col[0], col[1], col[2]);
			break;
		case 29:
			outalpha3 = atoi(token);
			if (outalpha3 != 255) outcolor3 = Gdiplus::Color(outalpha3, col[0], col[1], col[2]);
			break;
		case 30:
			outthikn3 = atoi(token);
			break;
		case 31:
			shaoffx = atoi(token);
			break;
		case 32:
			shaoffy = atoi(token);
			break;
		case 33:
			shadiffuse = atoi(token) == 1 ? true : false;
			break;
		case 34:
			shaextrude = atoi(token) == 1 ? true : false;
			break;
		default:
			break;
		}
		nTokenNum++;
		token = strtok(NULL, "\n");
	}
}

void __cdecl RepaintComponent( HWND win, char* kstr) 
{
	LOG(_);
	CIRPluginObject* cip = guiobj;
	if (cip == NULL) return;
	if (kstr != NULL) {
		delete guiobj->m_szProperties;
		guiobj->m_szProperties = strdup(kstr);
		guiobj->SetCustomProperties(kstr);
	}
	RECT r;
	GetWindowRect(win, &r);
	r.right = r.right - r.left;
	r.bottom = r.bottom - r.left;
	r.top = 0;
	r.left = 0;
	HDC ddc = GetDC(win);
	Gdiplus::Graphics* graphics = Gdiplus::Graphics::FromHDC(ddc);
	graphics->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeAntiAlias);
	graphics->SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic);
	Gdiplus::Color m_clrBkgd(255, 255, 255, 255);
	Gdiplus::Color m_clrEdge(255, 220, 220, 220);
	Gdiplus::SolidBrush brushBkgnd(m_clrBkgd);
	Gdiplus::HatchBrush hatch(Gdiplus::HatchStyle::HatchStyleLargeCheckerBoard, m_clrEdge, m_clrBkgd);
	graphics->FillRectangle(&brushBkgnd, r.left, r.top, r.right - r.left, 200);
	Gdiplus::Pen pen(m_clrEdge,1.0F);
	Gdiplus::Rect rct;
	rct.X = 10;
	rct.Y = 10;
	rct.Width = r.right - 21;
	rct.Height = 190;
	graphics->FillRectangle(&hatch, rct);
	graphics->DrawRectangle(&pen, rct);
	r.left += 10;
	r.top += 10;
	r.right -= 20;
	r.bottom -= 20;
	cip->DrawDesign(ddc, win, r, 1, 1);
	ReleaseDC(win, ddc);
}

BOOL CIRPluginObject::ShowProperties(char* szPluginFolder) {

	LOG(_);
	return FALSE;
}

BOOL CIRPluginObject::GetEvent(int nIndex, IRPluginEventInfo* pEventInfo)
{
	LOG(_);
	if ((nIndex >= 0) && nIndex < NUM_OB_EVENTS)
	{
		if (m_pEvents[nIndex])
		{
			lstrcpy(pEventInfo->m_szName, m_pEvents[nIndex]->m_szName);
			lstrcpy(pEventInfo->m_szArgs, m_pEvents[nIndex]->m_szArgs);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return NULL;
	}
}

int CIRPluginObject::GetNumEvents()
{
	LOG(_);
	return NUM_OB_EVENTS;
}

int CIRPluginObject::RegisterLUAFunctions(lua_State* L)
{
	LOG(_);
	m_pLuaState = L;
	return 0;
}

void CIRPluginObject::FireEvent(LPCTSTR strEventName, LPCTSTR strArguments)
{
	if (!m_pLuaState) return;

	lua_getglobal(m_pLuaState, "Page");
	lua_pushstring(m_pLuaState, "FireEvent");
	lua_gettable(m_pLuaState, -2);
	lua_remove(m_pLuaState, -2);

	if (lua_isfunction(m_pLuaState, -1))
	{
		lua_pushnumber(m_pLuaState, (double)((DWORD)this));
		lua_pushstring(m_pLuaState, strEventName);
		lua_pushstring(m_pLuaState, strArguments);
		if (lua_pcall(m_pLuaState, 3, 0, 0) != 0)
		{
			// Error
			lua_remove(m_pLuaState, -1);
		}
	}
	else
	{
		lua_remove(m_pLuaState, -1);
	}
}

void CIRPluginObject::DoSetFocus()
{
	LOG(_);
}

int CIRPluginObject::GetObjectID(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	int nLength = lstrlen(m_szObjectID);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, m_szObjectID);
		return nLength;
	}
}

CIRPluginObject* IRLUA_PLUGIN_GetObjectPtr(lua_State *luaState, LPCTSTR strObjectName)
{
	CIRPluginObject* pRet = NULL;
	DWORD dwObjectAddress;

	if (luaState)
	{
		lua_getglobal(luaState, "Page");
		lua_pushstring(luaState, "GetPluginObjectPtr");
		lua_gettable(luaState, -2);
		lua_remove(luaState, -2);

		if (lua_isfunction(luaState, -1))
		{
			lua_pushstring(luaState, strObjectName);
			if (lua_pcall(luaState, 1, 1, 0) != 0)
			{
				lua_remove(luaState, -1);
			}
			else
			{
				if (lua_isnumber(luaState, -1))
				{
					dwObjectAddress = (DWORD)lua_tonumber(luaState, -1);
					if (dwObjectAddress != 0)
					{
						pRet = (CIRPluginObject*)dwObjectAddress;
					}
				}
				lua_remove(luaState, -1);
			}
		}
		else
		{
			lua_remove(luaState, -1);
		}
	}

	return pRet;
}

void IRLUA_PLUGIN_RedrawObject(lua_State *luaState, LPCTSTR strObjectName)
{
	if (luaState)
	{
		lua_getglobal(luaState, "Page");
		lua_pushstring(luaState, "RedrawObject");
		lua_gettable(luaState, -2);
		lua_remove(luaState, -2);

		if (lua_isfunction(luaState, -1))
		{
			lua_pushstring(luaState, strObjectName);
			if (lua_pcall(luaState, 1, 0, 0) != 0)
			{
				lua_remove(luaState, -1);
			}
		}
		else
		{
			lua_remove(luaState, -1);
		}
	}
}