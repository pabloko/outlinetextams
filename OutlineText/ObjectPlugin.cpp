#include "stdafx.h"
#include "ObjectPlugin.h"


char _[1] = { '\0' };
char szInformation[] = "OutlineText Object for AutoPlay Media Studio\r\nCreated by Pabloko\r\nhttps://amsspecialist.com";
char szObjectName[] = "OutlineText";
char szVer[] = "1.0.0.0";
char szXML[1];

CIRPluginObject* guiobj;

OBJECTPLUGIN_API CIRPluginObject* irPlg_Object_CreateObject()
{
	LOG(_);
	if (m_gdiplusToken==NULL)
		Gdiplus::GdiplusStartup(&m_gdiplusToken, &m_gdiplusStartupInput, NULL);
	return new CIRPluginObject;
}

OBJECTPLUGIN_API void irPlg_Object_DeleteObject(CIRPluginObject* pObject)
{
	LOG(_);
	if (pObject)
	{
		delete pObject;
		pObject = NULL;
	}
}

void PrepareWindow(HWND h) {
	LOG(_);
	int veces = 0;
	Sleep(200);
	HWND win = FindWindowEx(h, NULL, NULL, "Settings");
	while (win == NULL) {
		Sleep(100);
		win = FindWindowEx(h, NULL, NULL, "Settings");
		veces++;
		if (veces > 30) 
			return;
	}
	HWND shit;
	shit = FindWindowEx(win, NULL, "Static", NULL);
	if (shit == NULL) {
		Sleep(100);
		shit = FindWindowEx(win, NULL, "Static", NULL);
	}
	::ShowWindow(shit, FALSE);
	//Log("we got shit on %x", shit);
	shit = FindWindowEx(win, NULL, "Edit", NULL);
	if (shit == NULL) {
		Sleep(100);
		shit = FindWindowEx(win, NULL, "Static", NULL);
	}
	::ShowWindow(shit, FALSE);
	//Log("we got shit on %x", shit);
	shit = FindWindowEx(win, NULL, "Button", NULL);
	::ShowWindow(shit, FALSE);
	//Log("we got shit on %x", shit);
	while (shit != NULL) {
		shit = FindWindowEx(win, shit, "Button", NULL);
		::ShowWindow(shit, FALSE);
		//Log("we got shit on %x", shit);
	}
	UpdateWindow(win);
	RepaintComponent(win,NULL);
}

OBJECTPLUGIN_API int irPlg_GetPluginName(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	if (guiobj != NULL) {
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)PrepareWindow, (void*)GetActiveWindow(), NULL, NULL);
		char ddldrive[6];
		char dllpath[MAX_PATH/2];
		char thisdll[MAX_PATH/2];
		GetModuleFileName((HINSTANCE)&__ImageBase, thisdll, _countof(thisdll));
		_splitpath(thisdll, ddldrive, dllpath,NULL,NULL);
		sprintf(thisdll, "%s%sOutlineTextGUI.dll", ddldrive,dllpath);
		delete thisdll;
		delete dllpath;
		delete ddldrive;
		HMODULE dl = GetModuleHandle(thisdll);
		if (dl == NULL) dl = LoadLibrary(thisdll);
		if (dl != NULL) {
			FARPROC fp = GetProcAddress(dl, "CreateForm");
			if (fp != NULL)
				(*(void(__cdecl **)(HWND, DWORD*, DWORD*, char*))&fp)(
					FindWindowEx(GetActiveWindow(), NULL, NULL, "Settings"),
					(DWORD*)guiobj,
					(DWORD*)RepaintComponent,
					guiobj->m_szProperties
				);
		}
	}

	int nLength = lstrlen(szObjectName);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szObjectName);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetPluginVersion(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	int nLength = lstrlen(szVer);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szVer);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetIRPluginObjectVersion()
{
	LOG(_);
	return 1;
}

OBJECTPLUGIN_API int irPlg_GetAuthorInfo(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	int nLength = lstrlen(szInformation);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szInformation);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetPluginActionXML(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	memset(szXML, 0, sizeof(szXML));
	int nLength = lstrlen(szXML);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, szXML);
		return nLength;
	}
}

OBJECTPLUGIN_API bool irPlg_ShowHelpForAction(char* lpszActionName, char* lpszPluginPath, HWND hParentWnd)
{
	LOG(_);
	CString strHelpFile;
	CString strFullHelpLink;

	strHelpFile.Format("%s\\Help.htm", lpszPluginPath);
	strFullHelpLink.Format("%s#%s", strHelpFile, lpszActionName);

	if (GetFileAttributes(strHelpFile) == -1)
	{
		return FALSE;
	}
	else
	{
		ShellExecute(hParentWnd, "open", strHelpFile, NULL, NULL, SW_NORMAL);
	}

	return TRUE;
}

OBJECTPLUGIN_API bool irPlg_ShowHelpForPlugin(char* lpszPluginPath, HWND hParentWnd)
{
	LOG(_);
	CString strHelpFile;

	strHelpFile.Format("%s\\Help.htm", lpszPluginPath);

	if (GetFileAttributes(strHelpFile) == -1)
	{
		return FALSE;
	}
	else
	{
		ShellExecute(hParentWnd, "open", strHelpFile, NULL, NULL, SW_NORMAL);
	}

	return TRUE;
}

OBJECTPLUGIN_API bool irPlg_ValidateLicense(char* lpszLicenseInfo)
{
	LOG(_);
	return TRUE;
}

OBJECTPLUGIN_API int irPlg_GetLuaVersion(char* szBuffer, int* pnBufferSize)
{
	LOG(_);
	int nLength = lstrlen(LUA_VERSION);
	if (*pnBufferSize < nLength)
	{
		*pnBufferSize = nLength;
		return -1;
	}
	else
	{
		memset(szBuffer, 0, *pnBufferSize);
		lstrcpy(szBuffer, LUA_VERSION);
		return nLength;
	}
}

OBJECTPLUGIN_API int irPlg_GetSDKVersion(void)
{
	LOG(_);
	return 2;
}


void Log(CHAR * szFormat, ...)
{
#ifndef NDEBUG
	char tmp_buf[512];
	memset(tmp_buf, 0, 512);

	va_list args;
	va_start(args, szFormat);
	vsprintf(tmp_buf, szFormat, args);
	va_end(args);

	OutputDebugString(tmp_buf);
#endif
}
