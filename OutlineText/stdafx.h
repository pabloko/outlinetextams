// stdafx.h: archivo de inclusi�n de los archivos de inclusi�n est�ndar del sistema
// o archivos de inclusi�n espec�ficos de un proyecto utilizados frecuentemente,
// pero rara vez modificados
//

#pragma once

// La inclusi�n de SDKDDKVer.h define la plataforma Windows m�s alta disponible.

// Si desea compilar la aplicaci�n para una plataforma Windows anterior, incluya WinSDKVer.h y
// establezca la macro _WIN32_WINNT en la plataforma que desea admitir antes de incluir SDKDDKVer.h.

#include <SDKDDKVer.h>

#define WIN32_LEAN_AND_MEAN             // Excluir material rara vez utilizado de encabezados de Windows
// Archivos de encabezado de Windows:
#include <windows.h>
#include <locale.h>
#include <atlstr.h>
#include "lua.hpp"
#include "IRPluginObject.h"
#include <Shellapi.h>
#include <Wincrypt.h>
//#include <Gdiplus.h>
#include "OutlineText.h"
extern void Log(CHAR * szFormat, ...);
extern void RepaintComponent(HWND win, char* kstr);
extern Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
extern ULONG_PTR m_gdiplusToken;
extern CIRPluginObject* guiobj;
extern char _[1];
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define LOG(...) Log("Trace: %s [%s](%d)",__FUNCTION__,__FILENAME__,__LINE__); if (#__VA_ARGS__>0) Log(__VA_ARGS__)
#ifdef NDEBUG
#define LOG(x) {}
#endif
// TODO: mencionar aqu� los encabezados adicionales que el programa necesita
