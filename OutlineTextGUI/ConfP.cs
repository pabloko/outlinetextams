﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OutlineTextGUI
{
    [RefreshProperties(System.ComponentModel.RefreshProperties.All)]
    [ReadOnly(false)]
    public class ConfP
    {
        private OTGUIF _form;
        public ConfP(OTGUIF form)
        {
            _form = form;
            fromStr(form.gtext_ptr);
        }
        public bool eventing = false;
        public void redraw()
        {
            if (eventing)
                _form.gfp(_form.parenthwnd, toStr());
        }

        private string _text;
        private Color _color1;
        private Color _color2;
        private _enum_gradientmode _gradto;
        /*private byte _fsize;
        private string _fname;
        private byte _fstyle;*/
        private bool _hasgradient;
        private _enum_mode _mode;
        /*private byte _alpha1;
        private byte _alpha2;*/
        private Color _outcolor1;
        private byte _outalpha1;
        private byte _outthikn1;
        private Color _outcolor2;
        private byte _outalpha2;
        private byte _outthikn2;
        private bool _shaen;
        private Color _outcolor3;
        private byte _outalpha3;
        private byte _outthikn3;
        /*private int _shaoffx;
        private int _shaoffy;*/
        private bool _shadiffuse;
        private bool _shaextrude;

        [Category("Generic Options")]
        [DisplayName("Text")]
        [Description("Set the text to display")]
        public string text
        {
            get { return _text; }
            set { _text = value; redraw(); }
        }

        public enum _enum_mode
        {
            TextGlow,
            SingleOutline,
            DblOutline,
            GradOutline,
            NoOutline,
            OnlyOutline,
            DblGlow
        }

        [Category("Generic Options")]
        [DisplayName("Mode")]
        [Description("Set the effect to apply, currently supported modes are:\nTextGlow,SingleOutline,DblOutline,GradOutline,NoOutline,OnlyOutline,DblGlow\nModes define how many outlines to use.")]
        public _enum_mode mode
        {
            get { return _mode; }
            set { _mode = value; redraw(); }
        }

        private Font _ft;
        [Category("Text Rendering")]
        [DisplayName("Font")]
        [Description("Font used to render text and its characteristics (note: not all combinations of FontStyles may work as GDI+ is limited to these styles: FontStyleRegular, FontStyleBold, FontStyleItalic, FontStyleBoldItalic, FontStyleUnderline, FontStyleStrikeout")]
        public Font fname
        {
            get { return _ft; }
            set { _ft = value; redraw(); }
        }

        [Category("Text Rendering")]
        [DisplayName("ColorPrimary")]
        [Description("Default text color")]
        public Color color1
        {
            get { return _color1; }
            set { _color1 = value; redraw(); }
        }

        [Category("Text Rendering")]
        [DisplayName("ColorSecondary")]
        [Description("Secondary text color for use on gradients")]
        public Color color2
        {
            get { return _color2; }
            set { _color2 = value; redraw(); }
        }

        [Category("Text Rendering")]
        [DisplayName("GradientEnable")]
        [Description("Use gradient color to render text")]
        public bool hasgradient
        {
            get { return _hasgradient; }
            set { _hasgradient = value; redraw(); }
        }

        public enum _enum_gradientmode
        {
            Horizontal,
            Vertical,
            ForwardDiagonal,
            BackwardDiagonal
        }
        [Category("Text Rendering")]
        [DisplayName("GradientMode")]
        [Description("Type of gradient to use")]
        public _enum_gradientmode gradto
        {
            get { return _gradto; }
            set { _gradto = value; redraw(); }
        }

        [Category("Outline 1")]
        [DisplayName("OutlineColor1")]
        [Description("Color for the outline 1")]
        public Color outcolor1
        {
            get { return _outcolor1; }
            set { _outcolor1 = value; redraw(); }
        }
        [Category("Outline 1")]
        [DisplayName("OutlineAlpha1")]
        [Description("Alpha component for the outline 1")]
        public byte outalpha1
        {
            get { return _outalpha1; }
            set { _outalpha1 = value; redraw(); }
        }
        [Category("Outline 1")]
        [DisplayName("OutlineThickness1")]
        [Description("Thickness for the outline 1")]
        public byte outthikn1
        {
            get { return _outthikn1; }
            set { _outthikn1 = value; redraw(); }
        }

        [Category("Outline 2")]
        [DisplayName("OutlineColor2")]
        [Description("Color for the outline 2")]
        public Color outcolor2
        {
            get { return _outcolor2; }
            set { _outcolor2 = value; redraw(); }
        }
        [Category("Outline 2")]
        [DisplayName("OutlineAlpha2")]
        [Description("Alpha component for the outline 2")]
        public byte outalpha2
        {
            get { return _outalpha2; }
            set { _outalpha2 = value; redraw(); }
        }
        [Category("Outline 2")]
        [DisplayName("OutlineThickness2")]
        [Description("Thickness for the outline 2")]
        public byte outthikn2
        {
            get { return _outthikn2; }
            set { _outthikn2 = value; redraw(); }
        }

        [Category("Shadow")]
        [DisplayName("ShadowEnabled")]
        [Description("Set text shadow enabled")]
        public bool shaen
        {
            get { return _shaen; }
            set { _shaen = value; redraw(); }
        }

        [Category("Shadow")]
        [DisplayName("ShadowDiffuse")]
        [Description("Set text shadow diffuse effect")]
        public bool shadiffuse
        {
            get { return _shadiffuse; }
            set { _shadiffuse = value; redraw(); }
        }
        [Category("Shadow")]
        [DisplayName("ShadowExtruded")]
        [Description("Set text shadow extruded effect")]
        public bool shaextrude
        {
            get { return _shaextrude; }
            set { _shaextrude = value; redraw(); }
        }

        private Point _pt;
        [Category("Shadow")]
        [DisplayName("ShadowOffset")]
        [Description("Set text shadow offset")]
        public Point shaoff
        {
            get { return _pt; }
            set { _pt = value; redraw(); }
        }

        [Category("Outline 3")]
        [DisplayName("ShadowColor")]
        [Description("Color for the shadow")]
        public Color outcolor3
        {
            get { return _outcolor3; }
            set { _outcolor3 = value; redraw(); }
        }
        [Category("Outline 3")]
        [DisplayName("ShadowAlpha")]
        [Description("Alpha component for the shadow")]
        public byte outalpha3
        {
            get { return _outalpha3; }
            set { _outalpha3 = value; redraw(); }
        }
        [Category("Outline 3")]
        [DisplayName("ShadowThickness")]
        [Description("Thickness for the shadow")]
        public byte outthikn3
        {
            get { return _outthikn3; }
            set { _outthikn3 = value; redraw(); }
        }

        public enum _enum_fs {
            FontStyleRegular = 0,
            FontStyleBold = 1,
            FontStyleItalic = 2,
            FontStyleBoldItalic = 3,
            FontStyleUnderline = 4,
            FontStyleStrikeout = 8
        }

        private void fromStr(string src)
        {
            string[] token = src.Split('\n');
            text = Encoding.Unicode.GetString(Convert.FromBase64String(token[0]));
            color1 = Color.FromArgb(byte.Parse(token[1]), byte.Parse(token[2]), byte.Parse(token[3]));
            color2 = Color.FromArgb(byte.Parse(token[4]), byte.Parse(token[5]), byte.Parse(token[6]));
            gradto = (_enum_gradientmode)byte.Parse(token[7]);
            byte fh = byte.Parse(token[10]);
            FontStyle fs = new FontStyle();
            switch ((_enum_fs)fh)
            {
                case _enum_fs.FontStyleBold:  fs |= FontStyle.Bold; break;
                case _enum_fs.FontStyleBoldItalic: fs |= FontStyle.Bold | FontStyle.Italic; break;
                case _enum_fs.FontStyleItalic: fs |= FontStyle.Italic; break;
                case _enum_fs.FontStyleRegular:  break;
                case _enum_fs.FontStyleStrikeout: fs |= FontStyle.Strikeout; break;
                case _enum_fs.FontStyleUnderline: fs |= FontStyle.Underline; break;
            }
            FontFamily fa = new FontFamily(token[9]);
            fname = new Font(fa, float.Parse(token[8]),fs);
            hasgradient = byte.Parse(token[11])==1?true:false;
            mode = (_enum_mode)byte.Parse(token[12]);

            outcolor1 = Color.FromArgb(byte.Parse(token[15]), byte.Parse(token[16]), byte.Parse(token[17]));
            outalpha1 = byte.Parse(token[18]);
            outthikn1 = byte.Parse(token[19]);

            outcolor2 = Color.FromArgb(byte.Parse(token[20]), byte.Parse(token[21]), byte.Parse(token[22]));
            outalpha2 = byte.Parse(token[23]);
            outthikn2 = byte.Parse(token[24]);

            shaen = byte.Parse(token[25]) == 1 ? true : false;

            outcolor3 = Color.FromArgb(byte.Parse(token[26]), byte.Parse(token[27]), byte.Parse(token[28]));
            outalpha3 = byte.Parse(token[29]);
            outthikn3 = byte.Parse(token[30]);

            shaoff = new Point(int.Parse(token[31]), int.Parse(token[32]));

            shadiffuse = byte.Parse(token[33]) == 1 ? true : false;
            shaextrude = byte.Parse(token[34]) == 1 ? true : false;
        }

        public string toStr()
        {
            string[] token = new string[35];
            try
            {
               
            token[0] = _form.EncryptBase64(text);
            token[1] = color1.R.ToString();
            token[2] = color1.G.ToString();
            token[3] = color1.B.ToString();
            token[4] = color2.R.ToString();
            token[5] = color2.G.ToString();
            token[6] = color2.B.ToString();
            token[7] = ((byte)gradto).ToString();
            token[8] = fname.Size.ToString();
            token[9] = fname.FontFamily.Name;
            int fs = (int)_enum_fs.FontStyleRegular;
            if (fname.Bold && !fname.Italic) fs = (int)_enum_fs.FontStyleBold;
            if (!fname.Bold && fname.Italic) fs = (int)_enum_fs.FontStyleItalic;
            if (fname.Bold && fname.Italic) fs = (int)_enum_fs.FontStyleBoldItalic;
            if (fname.Strikeout) fs = (int)_enum_fs.FontStyleStrikeout;
            if (fname.Underline) fs = (int)_enum_fs.FontStyleUnderline;
            token[10] = fs.ToString();
            token[11] = hasgradient?"1":"0";
            token[12] = ((byte)mode).ToString();

            token[13] = "255";
            token[14] = "255";

            token[15] = outcolor1.R.ToString();
            token[16] = outcolor1.G.ToString();
            token[17] = outcolor1.B.ToString();
            token[18] = outalpha1.ToString();
            token[19] = outthikn1.ToString();

            token[20] = outcolor2.R.ToString();
            token[21] = outcolor2.G.ToString();
            token[22] = outcolor2.B.ToString();
            token[23] = outalpha2.ToString();
            token[24] = outthikn2.ToString();

            token[25] = shaen ? "1" : "0";

            token[26] = outcolor3.R.ToString();
            token[27] = outcolor3.G.ToString();
            token[28] = outcolor3.B.ToString();
            token[29] = outalpha3.ToString();
            token[30] = outthikn3.ToString();

            token[31] = shaoff.X.ToString();
            token[32] = shaoff.Y.ToString();

            token[33] = shadiffuse ? "1" : "0";
            token[34] = shaextrude ? "1" : "0";
            }
            catch (Exception e) { MessageBox.Show(e.StackTrace); }
            return string.Join("\n",token).Trim();
        }
    }
}
