﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using static OutlineTextGUI.OTGUI;

namespace OutlineTextGUI
{
   

    public partial class OTGUIF : Form
    {
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;   
            public int Top;    
            public int Right;  
            public int Bottom; 
        }
        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, out RECT rectangle);

        public static int GWL_STYLE = -16;
        public static int WS_CHILD = 0x40000000;
        public IntPtr parenthwnd;
        public IntPtr parentparenthwnd;
        public IntPtr gptr;
        public FunctionPointerCb gfp;
        public string gtext_ptr;
        public ConfP cop;
        public OTGUIF(IntPtr hwnd, IntPtr ptr, FunctionPointerCb fp, string text_ptr)
        {
            gptr = ptr;
            gfp = fp;
            gtext_ptr = text_ptr;
            InitializeComponent();
            Hide();
            FormBorderStyle = FormBorderStyle.None;
            SetBounds(0, 0, 0, 0, BoundsSpecified.Location);
            parenthwnd = hwnd;
            parentparenthwnd = GetParent(hwnd);
            IntPtr hostHandle = hwnd;
            IntPtr guestHandle = this.Handle;
            RECT rc;
            GetWindowRect(hwnd, out rc);
            this.Width = (rc.Right - rc.Left) - 20;
            this.Height = (rc.Bottom - rc.Top) - 210;
            SetWindowLong(guestHandle, GWL_STYLE, GetWindowLong(guestHandle, GWL_STYLE) | WS_CHILD);
            SetParent(guestHandle, hostHandle);
            this.Dock = DockStyle.Fill;
            this.Location = new Point(10, 210);
            Show();
            Timer tm = new Timer();
            tm.Enabled = true;
            tm.Interval = 60;
            tm.Tick += Tm_Tick;
        }

        bool tab_visible = false;

        private void Tm_Tick(object sender, EventArgs e)
        {
            if (!IsWindowVisible(parenthwnd))
                tab_visible = false;
            else
            {
                if (!tab_visible)
                {
                    gfp(parenthwnd, null);
                    tab_visible = true;
                }
            }
            if (!IsWindowVisible(parentparenthwnd))
            {
                Close();
                Dispose();
                return;
            }
            RECT rc;
            GetWindowRect(parenthwnd, out rc);
            if (this.Width != (rc.Right - rc.Left) - 20 || this.Height != (rc.Bottom - rc.Top) - 210)
            {
                this.Width = (rc.Right - rc.Left) - 20;
                this.Height = (rc.Bottom - rc.Top) - 210;
                gfp(parenthwnd, null);
            }
            
        }

        public string EncryptBase64(string StringToEncrypt)
        {
            byte[] buffer = (new UnicodeEncoding()).GetBytes(StringToEncrypt);
            return System.Convert.ToBase64String(buffer);
        }

        private void OTGUIF_Load(object sender, EventArgs e)
        {
            cop = new ConfP(this);
            propertyGrid1.SelectedObject = cop;
            cop.eventing = true;
        }
    }
}
