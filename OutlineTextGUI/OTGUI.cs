﻿using RGiesecke.DllExport;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace OutlineTextGUI
{
    public class OTGUI
    {
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void FunctionPointerCb(IntPtr hwnd, string conf);

        [DllExport(CallingConvention = System.Runtime.InteropServices.CallingConvention.Cdecl)]
        public static void CreateForm(IntPtr hwnd, IntPtr ptr, FunctionPointerCb fp, string text_ptr)
        {
            /*OTGUIF form = */new OTGUIF(hwnd, ptr, fp, text_ptr);
        }
    }
}
